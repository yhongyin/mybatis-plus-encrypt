# mybatis-plus-encrypt

## 介绍

mybatis-plus-encrypt 是一款针对mybatis 的数据加密工具，原理基于mybatis的拦截器实现，包含以下功能

1. 保存/更新操作时，落库时对某字段进行加密
2. 查询操作时，对返回字段进行解密
3. 查询操作时，对加密字段的where条件参数进行加密处理

PS：为什么叫mybatis-plus-encrypt？一来是蹭热点，二来是多mybatis-plus做了特殊处理，其单表查询也支持加解密
## 背景

类似的工具网上有很多，实现起来其实并不难，重复造轮子主要有以下两点原因

1. mybatis-plus中也有相同的[插件](https://baomidou.com/pages/1864e1/) ,但居然是"收费版"，且不开源，其公开的jar包是被加密过处理过的，在企业开发中会有诸多不便
2. 市面上大多工具并不支持查询条件加密处理，即上面提到的第三点


## 使用

实体类与查询参数对象，需要继承Encrypted，在需要加解密的字段加上注解@Encrypt
```java
@Data
@Accessors(chain = true)
@TableName(value = "user")
public class UserEntity implements Encrypted {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Encrypt
    private String mobile;
}
```



## 关于查询操作时，对加密字段的条件参数进行加密处理的说明

在日常查询时中，如根据手机号查出某用户，在查询前，别的工具往往需要先硬编码加密
```java
String mobile="131xxxxxx";
mobile= 加密(mobile);

User user=new User();
user.setMobile(mobile);

xxService.getOne(user);

```
一点都不优雅，本工具支持自动对条件进行加密，支持以下的查询方式：

1. 支持mybatis-plus的单表查询：xxService.lambdaQuery() 、xxService.query()
2. 支持原生mybatis mapper 查询，入参为单一对象的方法： User query(xxDTO dto)
3. 原生mybatis mapper 查询，入参为字符串
```java
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {

    /**
     * 入参为字符串
     */
    @Select("select * from user where mobile=#{mobile}")
    UserEntity getByPhone1(@Encrypt @Param("mobile") String mobile);

    /**
     * 入参为对象
     */
    UserEntity getByPhone2(UserQueryDTO dto);

}
```


# Demo

基本操作都在demo项目里，附上一份测试DDL
```sql
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

```