package com.heys1.demo.controller;

import com.heys1.demo.mybatis.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yejunxi 2022/09/27
 */
@RestController
public class TestController {
    @Autowired
    UserService userService;

    @GetMapping("/test")
    public Object aVoid() {
        return userService.list();
    }
}
