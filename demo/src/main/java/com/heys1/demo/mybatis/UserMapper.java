package com.heys1.demo.mybatis;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heys1.support.encrypt.annotation.FieldEncrypt;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author yejunxi 2022/09/26
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {

    /**
     * 入参为字符串
     */
    @Select("select * from user where mobile=#{mobile}")
    List<UserEntity> listByPhone1(@FieldEncrypt @Param("mobile") String mobile);

    /**
     * 入参为对象
     */
    List<UserEntity> listByPhone2(UserQueryDTO dto);

}
