package com.heys1.demo.mybatis;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.heys1.support.encrypt.annotation.FieldEncrypt;
import com.heys1.support.encrypt.bean.Encrypted;
import com.heys1.support.sensitive.annotation.FieldSensitive;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author yejunxi 2022/09/26
 */
@Data
@Accessors(chain = true)
@TableName(value = "user")
public class UserEntity implements Encrypted {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @FieldEncrypt
    @FieldSensitive
    private String mobile;
}
