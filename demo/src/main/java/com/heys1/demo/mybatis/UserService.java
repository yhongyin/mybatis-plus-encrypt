package com.heys1.demo.mybatis;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author yejunxi 2022/09/26
 */
@Service
public class UserService extends ServiceImpl<UserMapper, UserEntity> {
}
