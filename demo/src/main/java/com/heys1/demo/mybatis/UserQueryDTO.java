package com.heys1.demo.mybatis;

import com.heys1.support.encrypt.annotation.FieldEncrypt;
import com.heys1.support.encrypt.bean.Encrypted;
import lombok.Data;

/**
 * @author yejunxi 2022/09/26
 */
@Data
public class UserQueryDTO implements Encrypted {

    @FieldEncrypt
    private String mobile;
}
