package com.heys1;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.HashMap;

@SpringBootApplication
@Slf4j
@MapperScan("com.heys1.*")
public class TestApplication {
    public static void main(String[] args) {
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("application.yml");
        HashMap ymlMap = new Yaml().loadAs(inputStream, HashMap.class);

        SpringApplication sa = new SpringApplication(TestApplication.class);
        sa.setDefaultProperties(ymlMap);
        sa.run(args);
    }
}
