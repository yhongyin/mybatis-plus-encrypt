import cn.hutool.json.JSONUtil;
import com.heys1.TestApplication;
import com.heys1.demo.mybatis.UserEntity;
import com.heys1.demo.mybatis.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@Slf4j
@ActiveProfiles("test")
@ComponentScan("cn.com.dragonpass.uop")
public class JunitTest {

    @Autowired
    UserService userService;

    @Test
    public void save() {
        userService.save(
                new UserEntity()
                        .setMobile("123")
        );
    }

    @Test
    public void query1() {
        List<UserEntity> list = userService.list();
        System.out.println(JSONUtil.toJsonStr(list));
    }

    @Test
    public void query2() {
        List<UserEntity> list = userService.getBaseMapper().listByPhone1("123");
        System.out.println(JSONUtil.toJsonStr(list));
    }
}