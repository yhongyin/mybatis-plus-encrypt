package com.heys1.support.encrypt.config;

import com.heys1.support.encrypt.handlers.DefaultEncryptor;
import com.heys1.support.encrypt.interceptor.EncryptionQueryInterceptor;
import com.heys1.support.encrypt.interceptor.EncryptionResultInterceptor;
import com.heys1.support.encrypt.interceptor.EncryptionSaveInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * @author yejunxi 2022/09/23
 */
@Lazy
@Configuration(
        proxyBeanMethods = false
)
public class EncryptAutoConfiguration {

    @ConditionalOnMissingBean
    @Bean
    public DefaultEncryptor defaultEncryptor() {
        return new DefaultEncryptor();
    }


    @Bean
    public EncryptionResultInterceptor encryptionResultInterceptor(DefaultEncryptor defaultEncryptor) {
        return new EncryptionResultInterceptor(defaultEncryptor);
    }

    @Bean
    public EncryptionQueryInterceptor encryptionQueryInterceptor(DefaultEncryptor defaultEncryptor) {
        return new EncryptionQueryInterceptor(defaultEncryptor);
    }

    @Bean
    public EncryptionSaveInterceptor encryptionSaveInterceptor(DefaultEncryptor defaultEncryptor) {
        return new EncryptionSaveInterceptor(defaultEncryptor);
    }
}
