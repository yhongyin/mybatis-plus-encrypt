package com.heys1.support.encrypt.handlers;

/**
 * @author yejunxi 2022/09/23
 */
public interface IEncryptor {

    String encrypt(String str);

    String decrypt(String str);
}
