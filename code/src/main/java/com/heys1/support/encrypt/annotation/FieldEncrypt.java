package com.heys1.support.encrypt.annotation;

import java.lang.annotation.*;

/**
 * 加密
 *
 * @author yejunxi
 */
@Documented
@Inherited
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldEncrypt {

}
