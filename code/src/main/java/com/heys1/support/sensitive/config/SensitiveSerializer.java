package com.heys1.support.sensitive.config;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;
import com.heys1.support.sensitive.strategy.ISensitiveStrategy;

import java.io.IOException;

public final class SensitiveSerializer extends StdScalarSerializer<Object> {
    private final ISensitiveStrategy strategy;


    public SensitiveSerializer(ISensitiveStrategy strategy) {
        super(String.class, false);
        this.strategy = strategy;
    }


    @Override
    public boolean isEmpty(SerializerProvider prov, Object value) {
        return value == null || StrUtil.isEmpty((String) value);
    }

    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        String content = strategy.handle((String) value);
        gen.writeString(content);
    }


}