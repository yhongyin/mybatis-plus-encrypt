package com.heys1.support.sensitive.annotation;

import com.heys1.support.sensitive.strategy.SensitiveStrategy;

import java.lang.annotation.*;

/**
 * 脱敏
 *
 * @author yejunxi
 */
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldSensitive {

    SensitiveStrategy strategy() default SensitiveStrategy.MIDDLE;
}