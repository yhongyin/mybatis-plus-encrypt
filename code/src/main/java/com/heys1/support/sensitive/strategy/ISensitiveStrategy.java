package com.heys1.support.sensitive.strategy;

public interface ISensitiveStrategy {

    String S_CHAR = "*";

    default String handle(String content) {
        return handle(content, S_CHAR);
    }

    String handle(String content, String maskChar);


}