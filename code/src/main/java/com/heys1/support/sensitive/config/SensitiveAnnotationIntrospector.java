package com.heys1.support.sensitive.config;

import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.NopAnnotationIntrospector;
import com.heys1.support.sensitive.annotation.FieldSensitive;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SensitiveAnnotationIntrospector extends NopAnnotationIntrospector {

    @Override
    public Object findSerializer(Annotated am) {
        FieldSensitive annotation = am.getAnnotation(FieldSensitive.class);
        if (annotation != null) {
            return new SensitiveSerializer(annotation.strategy().getStrategy());
        }
        return null;
    }
}